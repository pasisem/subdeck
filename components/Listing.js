import React from 'react';
import { View, ImageBackground, Text, Button, StyleSheet } from 'react-native';

import LoadingMessage from "./LoadingMessage";


const Listing = ({ data }) => {

    return (
        <View style={styles.listing}>
            {(data.thumbnail && data.thumbnail != "self" && data.thumbnail != "default") && <ImageBackground source={{ uri: data.thumbnail }} style={styles.bgImage}>
                <Text numberOfLines={1} style={styles.text}>{"u/" + data.author + " ^" + data.score}</Text>
                <Text numberOfLines={2} style={styles.title}>{data.title}</Text>
            </ImageBackground>}
            {(!data.thumbnail || data.thumbnail == "self" || data.thumbnail == "default") && <View>
                <Text numberOfLines={1} style={styles.text}>{"u/" + data.author + " ^" + data.score}</Text>
                <Text numberOfLines={2} style={styles.title}>{data.title}</Text>
            </View>}
        </View>
    )
};

const styles = StyleSheet.create({
    listing: {
        // backgroundColor: 'green',
        width: '100%',
        minHeight: 100,
        maxHeight: 300,
        borderBottomWidth: 1,
        borderBottomColor: '#000000',
    },
    title: {
        color: 'white',
        fontSize: 14,
    },
    text: {
        color: '#aaaaaa',
        fontSize: 12,
    },
    bgImage: {
        width: '100%',
        height: '100%'
    }
});

export default Listing;
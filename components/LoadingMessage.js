import React from 'react';
import { View, Text } from 'react-native';

const LoadingMessage = ({ subject }) => {

    return (
        <View>
            <Text>{"Loading " + subject + "..."}</Text>
        </View>
    );
};


LoadingMessage.defaultProps = {
    subject: "",
};

export default LoadingMessage;
import React, { useState } from 'react';
import { View, FlatList, Text, Button, StyleSheet } from 'react-native';

import LoadingMessage from "./LoadingMessage";
import Listing from "./Listing";


const Card = ({ sub }) => {
    const [data, setData] = useState();
    const [isLoading, setIsLoading] = useState(false);


    const getSub = () => {
        setIsLoading(true);
        fetch('https://www.reddit.com/r/' + sub + '.json', {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            }
        })

            .then(response => response.json())
            .then(json => setData(json))
            .catch((error) => console.error(error))
            .finally(() => setIsLoading(false));
    };

    return (
        <View style={styles.card}>
            <View style={styles.header}>
                <Text style={styles.title}>{"/r/" + sub}</Text>
                {!isLoading && <Button title={"load /r/" + sub} onPress={getSub}></Button>}
                {isLoading && <LoadingMessage subject={sub} />}
            </View>
            {data && <FlatList
                style={styles.list}
                data={data.data.children}
                keyExtractor={(child) => child.data.id}
                renderItem={({ item }) => (<Listing data={item.data} />)}
            />}
        </View>
    );
};


Card.defaultProps = {
    sub: "all",
};




const styles = StyleSheet.create({
    card: {
        width: 300,
        // height: '99%',
        margin: 5,
        backgroundColor: 'darkslateblue',
        borderRadius: 9,
        overflow: 'scroll',
        flex: 1,
        alignContent: 'center',
        justifyContent: 'flex-start',
        paddingBottom: 10
    },
    header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderBottomWidth: 1,
        borderBottomColor: '#000000',
    },
    title: {
        color: 'white',
        fontSize: 22,
        padding: 5,
        paddingLeft: 10
    },
    list: {
        borderBottomColor: "#000000",
        borderBottomWidth: 1
    }
});

export default Card;
import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, SafeAreaView, ScrollView, Button } from 'react-native';

import Card from './components/Card';

export default function App() {
  return (
    <SafeAreaView style={styles.container}>
      <StatusBar backgroundColor="red" barStyle="light-content" />
      <ScrollView horizontal={true}>
        <Card sub="all"></Card>
        <Card sub="popular"></Card>
        <Card sub="games"></Card>
        <Card sub="movies"></Card>
        <Card sub="television"></Card>
      </ScrollView>
    </SafeAreaView>
  );
}




const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#000033',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    paddingTop: Platform.OS === 'android' ? 35 : 0
  }
});
